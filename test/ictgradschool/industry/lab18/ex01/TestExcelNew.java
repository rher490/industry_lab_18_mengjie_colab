package ictgradschool.industry.lab18.ex01;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * TODO Write tests
 */
public class TestExcelNew {

    ExcelNew test;

    @Before
    public void testGoodCode() {
        test = new ExcelNew();
        test.start();
    }

    @Test
    public void testGenerateStudent() {
        test.generateStudent();
        File myFile = new File("Data_Out2.txt");

        try (Scanner myScanner = new Scanner(myFile)) {
            int size = 0;

            while (myScanner.hasNext()) {
                size++;
                String line = myScanner.nextLine();
                String[] lineArray = line.split("\t");
                System.out.println(Arrays.toString(lineArray));
                assertTrue(lineArray.length == 8);
            }

            assertTrue(size == 550);
        } catch (FileNotFoundException e) {
            fail();
            System.out.printf(e.getMessage());
        }
    }

}