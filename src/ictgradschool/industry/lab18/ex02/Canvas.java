package ictgradschool.industry.lab18.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by mshe666 on 16/01/2018.
 */
public class Canvas extends JPanel {

    private final int LEFT = 37;
    private final int RIGHT = 39;
    private final int DOWN = 40;
    private final int UP = 38;
    //
    private static final Random random = new Random();
    private int greenDotX = -1, greenDotY = -1;
    //
    private ArrayList<Integer> redDotXs = new ArrayList<>();
    private ArrayList<Integer> snakeXs = new ArrayList<>();
    private ArrayList<Integer> redDotYs = new ArrayList<>();
    private ArrayList<Integer> snakeYs = new ArrayList<>();

    private int direction = RIGHT;
    private boolean gameOver = false;
    private Program program;


    public Canvas(Program program) {

        this.program = program;

        //initializing xys of snake
        for (int i = 0; i < 6; i++) {
            snakeXs.add(10 - i);
            snakeYs.add(10);
        }

        System.out.println("middle of constructor");
        this.direction = RIGHT;

        addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                int newDirection = e.getKeyCode();
                if ((newDirection >= LEFT) && (newDirection <= DOWN)) {// block wrong codes
                    if (Math.abs(direction - newDirection) != 2) {// block moving back
                        direction = newDirection;
                    }
                }
            }
        });

//        repaint();
        this.setPreferredSize(new Dimension(700, 500));
//        this.setSize(new Dimension(30 * 25, 20 * 25));
        System.out.println(this.getHeight());
        System.out.println(this.getWidth());

//        go();
    }

    void go() { // main loop
        System.out.println("go ");
        while (!gameOver) {
            moveSnake();
            //snake is growing
            snakeIsGrowing();
            //generate a  new green dot
            if (greenDotX == -1) {
                System.out.println("green dot condition met");
                generateNewGreenDot();
                //generate a new red dot
                generateANewRedDot();
            }
            //

            this.repaint();
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void generateANewRedDot() {
        int newRedDotX, newRedDotY;
        boolean newRedDotIsOverExistingRedDots = false;
        boolean newRedDotIsOverSnake = false;
        do {
            newRedDotX = random.nextInt(30);
            newRedDotY = random.nextInt(20);
            for (int i = 0; i < redDotXs.size(); i++) {
                if (redDotXs.get(i) == newRedDotX && redDotYs.get(i) == newRedDotY) {
                    newRedDotIsOverExistingRedDots = true;
                }
            }
            for (int i = 0; i < snakeXs.size(); i++) {
                if ((snakeXs.get(i) == newRedDotX) && (snakeYs.get(i) == newRedDotY)) {
                    if (!((snakeXs.get(snakeXs.size() - 1) == newRedDotX) && (snakeYs.get(snakeYs.size() - 1) == newRedDotY))) {
                        newRedDotIsOverSnake = true;
                    }
                }
            }
        }
        while (newRedDotIsOverExistingRedDots || newRedDotIsOverSnake || greenDotX == newRedDotX && greenDotY == newRedDotY);

        redDotXs.add(newRedDotX);
        redDotYs.add(newRedDotY);
    }

    private void generateNewGreenDot() {
        int newGreenDotX, newGreenDotY;
        boolean isOverRedDots = false;
        boolean isOverSnake = false;
        do {
            newGreenDotX = random.nextInt(30);
            newGreenDotY = random.nextInt(20);
            for (int i = 0; i < redDotXs.size(); i++) {
                if (redDotXs.get(i) == newGreenDotX && redDotYs.get(i) == newGreenDotY) {
                    isOverRedDots = true;
                }
            }
            for (int i = 0; i < snakeXs.size(); i++) {
                if ((snakeXs.get(i) == newGreenDotX) && (snakeYs.get(i) == newGreenDotY)) {
                    if (!((snakeXs.get(snakeXs.size() - 1) == newGreenDotX) && (snakeYs.get(snakeYs.size() - 1) == newGreenDotY))) {
                        isOverSnake = true;
                    }
                }
            }
        } while (isOverSnake || isOverRedDots);

        greenDotX = newGreenDotX;
        greenDotY = newGreenDotY;
    }

    private void snakeIsGrowing() {
        if (((snakeXs.get(0) == greenDotX) && (snakeYs.get(0) == greenDotY))) {
            greenDotX = -1;
            greenDotY = -1;
            this.program.setTitle("Program current length of snake is" + " : " + snakeXs.size());
        } else {
            snakeXs.remove(snakeXs.size() - 1);
            snakeYs.remove(snakeYs.size() - 1);
        }
        System.out.println(greenDotX + " "+greenDotY);
    }

    private void moveSnake() {
        int snakeHeadX = snakeXs.get(0);

        int snakeHeadY = snakeYs.get(0);
        if (direction == LEFT) {
            snakeHeadX--;
        }
        if (direction == RIGHT) {
            snakeHeadX++;
        }
        if (direction == UP) {
            snakeHeadY--;
        }
        if (direction == DOWN) {
            snakeHeadY++;
        }

        //check if snake cross the boundary
        if (snakeHeadX > 30 - 1) {
            snakeHeadX = 0;
        }
        if (snakeHeadX < 0) {
            snakeHeadX = 30 - 1;
        }
        if (snakeHeadY > 20 - 1) {
            snakeHeadY = 0;
        }
        if (snakeHeadY < 0) {
            snakeHeadY = 20 - 1;
        }
        System.out.println("reach line 97");
        //check two gameOver conditions
        boolean snakeHitsRedDot = false;
        boolean snakeHitsItself = false;
        for (int i1 = 0; i1 < redDotXs.size(); i1++) {
            if (redDotXs.get(i1) == snakeHeadX && redDotYs.get(i1) == snakeHeadY) {
                snakeHitsRedDot = true;
            }
        }
        System.out.println("line 106");
        for (int i1 = 0; i1 < snakeXs.size(); i1++) {
            if ((snakeXs.get(i1) == snakeHeadX) && (snakeYs.get(i1) == snakeHeadY)) {
                if (!((snakeXs.get(snakeXs.size() - 1) == snakeHeadX) && (snakeYs.get(snakeYs.size() - 1) == snakeHeadY))) {
                    snakeHitsItself = true;
                }
            }
        }

        System.out.println("line 115");
        //
        gameOver = snakeHitsRedDot || snakeHitsItself;
        //changing snake head to new XY
        snakeXs.add(0, snakeHeadX);
        snakeYs.add(0, snakeHeadY);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        System.out.println(snakeXs.toString());
        System.out.println(snakeYs.toString());
        for (int i = 0; i < snakeXs.size(); i++) {
            g.setColor(Color.gray);
            g.fill3DRect(snakeXs.get(i) * 25 + 1, snakeYs.get(i) * 25 + 1, 25 - 2, 25 - 2, true);

        }
//        System.out.println(greenDotX + " : "+greenDotY);
//        System.out.println(redDotXs.toString());
//        System.out.println(redDotYs.toString());
        g.setColor(Color.green);
        g.fill3DRect(greenDotX * 25 + 1, greenDotY * 25 + 1, 25 - 2, 25 - 2, true);
        for (int i = 0; i < redDotXs.size(); i++) {
            g.setColor(Color.red);
            g.fill3DRect(redDotXs.get(i) * 25 + 1, redDotYs.get(i) * 25 + 1, 25 - 2, 25 - 2, true);
        }
        if (gameOver) {
            g.setColor(Color.red);
            g.setFont(new Font("Arial", Font.BOLD, 60));
            FontMetrics fm = g.getFontMetrics();
            g.drawString("Over", (30 * 25 + 6 - fm.stringWidth("Over")) / 2, (20 * 25 + 28) / 2);
        }
    }
}
