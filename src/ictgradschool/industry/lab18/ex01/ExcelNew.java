package ictgradschool.industry.lab18.ex01;

import java.io.*;
import java.util.*;

/**
 * TODO Please test & refactor this - my eyes are watering just looking at it :'(
 */
public class ExcelNew {

    private String student;
    private ArrayList<String> firstNameList = new ArrayList<String>();
    private ArrayList<String> surnameList = new ArrayList<String>();
    int classSize = 550;

    public static void main(String[] args) {
        ExcelNew excelNew = new ExcelNew();
        excelNew.start();
    }

    public void start() {

        //read from existing file
        try {
            importNameList(firstNameList, surnameList);
            String output = "";
            output = generateStudent();
            outputExcelNew(output);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    private void outputExcelNew(String output) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter("Data_Out2.txt"));
        bw.write(output);
        bw.close();
    }

    public String generateStudent() {
        String output = "";

        for (int i = 1; i <= classSize; i++) {
            student = getStudentID(i);

            int randFNIndex = (int) (Math.random() * firstNameList.size());
            int randSNIndex = (int) (Math.random() * surnameList.size());

            student += "\t" + surnameList.get(randSNIndex) + "\t" + firstNameList.get(randFNIndex) + "\t";

            //Student Skill
            int randomStudentIQ = (int) (Math.random() * 101);
            //Labs//////////////////////////
            int numLabs = 3;
            for (int j = 0; j < numLabs; j++) {
                addRandomMark(randomStudentIQ);
            }
            //Test/////////////////////////
            addRandomMark(randomStudentIQ);
            ///////////////Exam////////////
            addRandomExamMark(randomStudentIQ);
            //////////////////////////////////
            output += student + "\n";
        }
        return output;
    }

    private String getStudentID(int i) {
        student = "";
        // studentID surname lastname
        if (i / 10 < 1) {
            student += "000" + i;
        } else if (i / 100 < 1) {
            student += "00" + i;
        } else if (i / 1000 < 1) {
            student += "0" + i;
        } else {
            student += i;
        }

        return student;
    }

    private void addRandomExamMark(int randomStudentIQ) {
        if (randomStudentIQ <= 7) {
            int randDNSProb = (int) (Math.random() * 101);
            if (randDNSProb <= 5) {
                student += "DNS"; //DNS
            } else {
                student += (int) (Math.random() * 40); //[0,39]
            }
        } else if ((randomStudentIQ > 7) && (randomStudentIQ <= 20)) {
            student += ((int) (Math.random() * 10) + 40); //[40,49]
        } else if ((randomStudentIQ > 20) && (randomStudentIQ <= 60)) {
            student += ((int) (Math.random() * 20) + 50);//[50,69]
        } else if ((randomStudentIQ > 60) && (randomStudentIQ <= 90)) {
            student += ((int) (Math.random() * 20) + 70); //[70,89]
        } else {
            student += ((int) (Math.random() * 11) + 90); //[90,100]
        }
    }

    private String addRandomMark(int randomStudentIQ) {
        if (randomStudentIQ <= 5) {
            student += (int) (Math.random() * 40); //[0,39]
        } else if ((randomStudentIQ > 5) && (randomStudentIQ <= 15)) {
            student += ((int) (Math.random() * 10) + 40); // [40,49]
        } else if ((randomStudentIQ > 15) && (randomStudentIQ <= 25)) {
            student += ((int) (Math.random() * 20) + 50); // [50,69]
        } else if ((randomStudentIQ > 25) && (randomStudentIQ <= 65)) {
            student += ((int) (Math.random() * 20) + 70); // [70,89]
        } else {
            student += ((int) (Math.random() * 11) + 90); //[90,100]
        }
        student += "\t";
        return student;
    }

    private static void importNameList(ArrayList<String> firstNameList, ArrayList<String> surnameList) throws IOException {
        String line;
        BufferedReader br = new BufferedReader(new FileReader("FirstNames.txt"));
        while ((line = br.readLine()) != null) {
            firstNameList.add(line);
        }
        br.close();
        br = new BufferedReader(new FileReader("Surnames.txt"));
        while ((line = br.readLine()) != null) {
            surnameList.add(line);
        }
        br.close();
    }

}